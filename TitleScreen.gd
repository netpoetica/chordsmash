extends Control

signal start_game
signal show_instructions
signal show_credits

func open_menu(play_again: bool = false):
	self.show()
	if play_again:
		$MenuItemsColumn/PlayButton/Label.text = "Play Again"
	else:
		$MenuItemsColumn/PlayButton/Label.text = "Play"
	$MenuItemsColumn/PlayButton.connect("pressed", self, "start_game")
	$MenuItemsColumn/InstructionsButton.connect("pressed", self, "show_instructions")
	$MenuItemsColumn/CreditsButton.connect("pressed", self, "show_credits")

func start_game():
	print("Starting game")
	emit_signal("start_game")

func show_instructions():
	print("Showing instructions")
	emit_signal("show_instructions")

func show_credits():
	print("Showing credits")
	emit_signal("show_credits")
	
func close_menu():
	self.hide()
	# cleanup unnecessary listeners
	$MenuItemsColumn/PlayButton.disconnect("pressed", self, "start_game")
	$MenuItemsColumn/InstructionsButton.disconnect("pressed", self, "show_instructions")
	$MenuItemsColumn/CreditsButton.disconnect("pressed", self, "show_credits")
