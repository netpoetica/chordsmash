extends Area2D

signal scored

func _on_Goal_body_shape_entered(body_id, body, body_shape, area_shape):
	var overlapping_bodies = self.get_overlapping_bodies()

	# bail immediately if this is just a single note
	if overlapping_bodies.size() < 3: return

	# print("Goal area entered", body)
	
	# clear previous
	var bodies_with_touch_ptr = []

	# collect all overlapping bodies at this exact moment that are grabbed
	for overlapping_body in overlapping_bodies:
		if overlapping_body.touch_ptr_id != null:
			bodies_with_touch_ptr.append(overlapping_body)

	# for now, triads only
	if bodies_with_touch_ptr.size() == 3:
		# print("3 touched bodies in goal")

		# we need to try to get the chord for each note, until we find the first match
		for body_with_touch_ptr in bodies_with_touch_ptr:
			# print("We've got a score!")

			# we need to figure out what chord even is this that the user just slammed together, if any?
			var scale = Scale.new()
			scale.use_scale(Scale.C_MAJOR)
			var triad = scale.get_triad_from_root(body_with_touch_ptr.note)
			# print("Is this a chord? " + triad[0] + triad[1] + triad[2])
			# print("Touched notes! " + bodies_with_touch_ptr[0].note + bodies_with_touch_ptr[1].note + bodies_with_touch_ptr[2].note)
				
			# if this triad contains all of these notes, then play
			if (
				triad.has(bodies_with_touch_ptr[0].note)
				&& triad.has(bodies_with_touch_ptr[1].note)
				&& triad.has(bodies_with_touch_ptr[2].note)
			):
				# print("Playing a chord! " + triad[0] + triad[1] + triad[2])
				# print("Touched notes! " + bodies_with_touch_ptr[0].note + bodies_with_touch_ptr[1].note + bodies_with_touch_ptr[2].note)
				emit_signal("scored", triad)
				# play all notes sounds. Note: this isn't going to work. 
				# we need to use triad.play() method once defined
				# right now these are technically inversions, because it is all one octave
				for body in bodies_with_touch_ptr:
					# body.play_sound()
					body.queue_free()
				
				var chords_data = ChordsDataObject.new()
				var chord = chords_data.get_chord_by_notes_arr(triad)
				AudioManager.play_notes(chord.get_notes())

				# once we've found the chord, bail
				return
