extends RigidBody2D

func set_display(triad: Array):
	$TopNoteDisplay.set_note(triad[0])
	$MiddleNoteDisplay.set_note(triad[1])
	$BottomNoteDisplay.set_note(triad[2])

func reset_note_displays():
	$TopNoteDisplay.hide()
	$TopNoteDisplay.current_note = null
	$MiddleNoteDisplay.hide()
	$MiddleNoteDisplay.current_note = null
	$BottomNoteDisplay.hide()
	$BottomNoteDisplay.current_note = null

# If the Birdie is targeting C E G, they will sing C first
# C will be the bottom note
# her next note will be an E, and then bottom becomes middle and E becomes bottom
func set_next_available_note(note: String):
	if $BottomNoteDisplay.current_note == null:
		# bottom is open
		$BottomNoteDisplay.show()
		$BottomNoteDisplay.set_note(note)
		return
	else:
		if $MiddleNoteDisplay.current_note == null:
			# middle is open
			$MiddleNoteDisplay.set_note($BottomNoteDisplay.current_note)
			$MiddleNoteDisplay.show()
			$BottomNoteDisplay.set_note(note)
			return
		else:
			if $TopNoteDisplay.current_note == null:
				# top is open
				$TopNoteDisplay.set_note($MiddleNoteDisplay.current_note)
				$TopNoteDisplay.show()
				$MiddleNoteDisplay.set_note($BottomNoteDisplay.current_note)
				$BottomNoteDisplay.set_note(note)
				return
	# print("No open positions")