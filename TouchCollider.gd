extends Area2D

signal note_grabbed

var touch_ptr_id: int
var current_note: Node

# when 3 touch colliders smash into 
# each other, produce a chord
# and add score!
var collided_notes = []

const OFFSCREEN_TOUCH_COLLIDER_POSITION = Vector2(-9999.00, -9999.00)

func _physics_process(delta):
	if current_note != null:
		if is_instance_valid(current_note) && position != OFFSCREEN_TOUCH_COLLIDER_POSITION:
			current_note.position = position

func set_touch_ptr_id(ptr_id):
	touch_ptr_id = ptr_id

func _on_TouchCollider_body_entered(body):
	# print("body entered", body)
	# body_entered_time = OS.get_unix_time()
	if current_note == null && "TouchNote" in body.get_name():
		# print("Grabbing a note")
		var note = body
		current_note = note
		note.grab(touch_ptr_id, position)
	# if we hit another collider and we have a note under our finger
	elif "TouchCollider" in body.get_name() && current_note != null:
		# print("Touch Colliders collide!")
		collided_notes.append(body.current_note)

func _on_TouchCollider_body_exited(body):
	var body_exit_time = OS.get_unix_time()
	if "TouchNote" in body.get_name():
		var note = body
		if current_note == note:
			note.release(touch_ptr_id)
			current_note = null
