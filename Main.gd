extends Node2D
onready var TouchCollider = preload("res://TouchCollider.tscn")
onready var TouchNote = preload("res://TouchNote.tscn")

class_name Main

#
# Game State
#
var game_started: bool = false
# the birdie will not start singing until the player has gotten some points
var first_score_occurred: bool = false

# Current score
var score = 0

# these are the chords that are currently available to target (visible in the display)
const NUM_TARGET_CHORDS = 4 # 4 targets in the HUD
var target_chords = []

# every time birdie whistles, store that note in here. We use this in order
# to search for other potential matching chords in target_chords at the time birdie
# is about to sing her third note and make a triad
var birdie_notes = []
var birdie_current_target_chord_index = null
var birdie_current_note_of_target_chord_index = 0

# Get the touch helper singleton
var touch_helper = null

# get the audio manager singleton
var audio_manager = null

# total number of fingers
const MAX_COLLISION_SHAPES = 5
var collisionShapesRegistry = {}

const OFFSCREEN_TOUCH_COLLIDER_POSITION = Vector2(-9999.00, -9999.00)

# we will push/pop touch colliders onto this queue
# and utilize as needed (per touch, finger)
var collisionShapes = []

const MAX_NOTES_PER_SPAWN_INTERVAL = 9
const MIN_NOTES_PER_SPAWN_INTERVAL = 3

# Different score values
const SCORE_FREE_CHORD = 1
const SCORE_SINGLE_TARGET_CHORD = 10
const SCORE_ALL_TARGET_CHORDS = 100

var chords_data = ChordsDataObject.new()

func _ready():
	# seed random num generator
	randomize()

	# initially hide the hud as TitleScreen/Menu is open
	# Menu should be open initially
	open_main_menu()
	
	# birdie's notes shouldn't be visible until birdie whistles
	reset_birdie()

	setup_target_chords()
	setup_hud_signals()

	touch_helper = get_node("/root/TouchHelper")
	audio_manager = get_node("/root/AudioManager")

	# print("Target Chords initialalized. Length: " + String(target_chords.size()))

	# whenever touch helper reports a release, see if we have it in our
	# shape registry and if so, remove it
	touch_helper.node.connect("touch_released", self, "_remove_touch")

	setup_collision_shapes()

func setup_target_chords():
	target_chords = []
	# godot doesn't let you instantiate arrays of specific size
	target_chords.resize(NUM_TARGET_CHORDS)

func setup_collision_shapes():
	# initialize all touch colliders (for each finger)
	for n in range(0, MAX_COLLISION_SHAPES):
		var touchCollider = TouchCollider.instance()
		# initialize off screen
		touchCollider.position = OFFSCREEN_TOUCH_COLLIDER_POSITION
		# add to the queue
		collisionShapes.append(touchCollider)
		add_child(touchCollider)

func start_game() -> void:
	game_started = true
	first_score_occurred = false
	
	# hud should be visible now and should remain so even if the menu opens again
	# remove unnecessary event/signal listeners
	close_main_menu()

	# reset score
	reset_score()

	# start the background music
	audio_manager.play_music("res://sfx/background/c-pad.wav")
	
	# Cute startup sound, maybe remove?
	var c_chord = chords_data.get_chord_by_name("C Major")
	audio_manager.play_notes(c_chord.get_notes())

	$NoteSpawnTimer.start()
	create_target_chords()
	
	start_birdie()

func end_game() -> void:
	# show main menu w/ play again message
	open_main_menu(true)
	AudioManager.stop_all()
	$NoteSpawnTimer.stop()
	$Birdie/BirdWhistleTimer.stop()
	$HUD.clear_chords()
	for node in get_children():
		if "TouchNote" in node.get_name():
			remove_child(node)

func _process(_delta):
	# To keep redrawing on every frame
	update()

func _remove_touch(ptr_id):
	# if there is a registered element for which there is no longer a touch
	# then remove it from the registry and add it back to the pool
	if collisionShapesRegistry.has(ptr_id):
		# get it
		var collisionShape = collisionShapesRegistry.get(ptr_id)
		# move it offscreen
		collisionShape.position = OFFSCREEN_TOUCH_COLLIDER_POSITION
		# erase it from registry
		collisionShapesRegistry.erase(ptr_id)
		# add it back to the pool
		collisionShapes.append(collisionShape)

func _draw():
	# Draw every touch collider
	for ptr_id in touch_helper.state.keys():
		var pos = touch_helper.state[ptr_id]

		# try to find a current touch for this ptr_id
		var collisionShape = collisionShapesRegistry.get(ptr_id)
		if collisionShape == null:
			# if you didn't find one, take one from the pool
			collisionShapesRegistry[ptr_id] = collisionShapes.pop_front()
			# use it
			collisionShape = collisionShapesRegistry[ptr_id]

		# set its position
		# could still be null if they went above max touches
		if collisionShape != null:
			collisionShape.set_touch_ptr_id(ptr_id)
			collisionShape.position = pos

func _on_NoteSpawnTimer_timeout():
	for i in rand_range(MIN_NOTES_PER_SPAWN_INTERVAL, MAX_NOTES_PER_SPAWN_INTERVAL):
		$NotePath/NotePathFollow.set_offset(randi())
		var touch_note = TouchNote.instance()
		add_child(touch_note)
		# set direction perpendicular to the path direction
		var direction = $NotePath/NotePathFollow.rotation + PI / 2
		var screen_size = get_viewport().get_size()
		# start in the middle of the screen
		# var spawn_position = Vector2(screen_size.x / 2, screen_size.y /2)
		var spawn_position = $NotePath/NotePathFollow.position
		touch_note.position = spawn_position
		direction += rand_range(-PI / 4, PI / 4)
		touch_note.rotation = direction
		# set velocity (speed + direction)
		touch_note.linear_velocity = Vector2(rand_range(touch_note.min_speed, touch_note.max_speed), 0)
		touch_note.linear_velocity = touch_note.linear_velocity.rotated(direction)
		# audio_manager.play_note(touch_note.note, 3, "SpawnBus")

#
# MAIN MENU
#
func open_main_menu(show_play_again_text: bool = false) -> void:
	hide_hud()
	# if it's the first time, it s
	$Menu.open_menu(show_play_again_text)
	setup_menu_signals()

func close_main_menu() -> void:
	show_hud()
	remove_menu_signals()
	$Menu.close_menu()

func setup_menu_signals():
	$Menu.connect("start_game", self, "start_game")
	$Menu.connect("show_instructions", self, "show_instructions")
	$Menu.connect("show_credits", self, "show_credits")

func remove_menu_signals():
	$Menu.disconnect("start_game", self, "start_game")
	$Menu.disconnect("show_instructions", self, "show_instructions")
	$Menu.disconnect("show_credits", self, "show_credits")
#
# END MAIN MENU
#

#
# HUD
#
func setup_hud_signals():
	$HUD.connect("help_requested", self, "show_instructions")

# canvas layers can't have visibility, so I need a little trick to hide when the menu is open
func show_hud():
	$HUD.layer = 2

func hide_hud():
	$HUD.layer = -1

# add 4 chords (aka a "level") to HUD
func create_target_chords():
	print("Creating target chords", target_chords)
	clear_target_chords()
	var triads = populate_target_chords()
	$HUD.set_chords(triads)

func populate_target_chords():
	var triads = []

	for i in range(0, NUM_TARGET_CHORDS):
		var scale = Scale.new()
		scale.use_scale(Scale.C_MAJOR)
		# use a different root letter every time
		var root_note = scale.note_letters[randi() % scale.note_letters.size()]
		var triad = scale.get_triad_from_root(root_note)
		triads.append(triad)
		# save a reference in target_chords so we can check upon scoring
		target_chords[i] = triad
		print("Setting target_chord at index " + String(i), target_chords)

	return triads
#
# END HUD
#

#
# TARGET CHORDS
#
func clear_target_chords():
	print("Clearing target chords", target_chords)
	target_chords = []
	target_chords.resize(NUM_TARGET_CHORDS)
	$HUD.clear_chords()

#
# INSTRUCTIONS
#
func show_instructions():
	hide_hud()
	$InstructionsOverlay.show()
	$InstructionsOverlay.connect("instructions_read", self, "hide_instructions")

func hide_instructions():
	if game_started:
		show_hud()
	$InstructionsOverlay.disconnect("instructions_read", self, "hide_instructions")
	$InstructionsOverlay.hide()

func show_credits():
	$Credits/Dialog.popup()
#
# END INSTRUCTIONS
#

#
# GOAL
#
func _on_Goal_scored(triad: Array) -> void:
	# print("Goal Scored! Target chords: ", target_chords)
	var max_index = target_chords.size() - 1
	# print("Max Index: " + String(max_index))
	if triad != null && triad.size() > 0:
		var found_target_chord_index = get_chord_index_in_target_chords(triad)
		if found_target_chord_index == -1:
			# print("Found chord is not in target chords. Max: " + String(max_index), target_chords)
			change_score_by(SCORE_FREE_CHORD)
		elif found_target_chord_index > max_index:
			#print("Invalid index! This should not be possible.")
			pass
		else:
			# print("Found chord at index " + String(found_target_chord_index) + ": ", target_chords[found_target_chord_index])
			# gotta keep these in sync
			$HUD.remove_chord_at_index(found_target_chord_index)
			# we can't simply remove(), we want this array to stay the right size
			# I do not like this. Needs a smarter, centralized game state storage.
			target_chords[found_target_chord_index] = null
			# print("Removed target_chord at index " + String(found_target_chord_index), target_chords)

			# if we finished the same chord birdie was already working on, then we need to reset birdie
			if found_target_chord_index == birdie_current_target_chord_index:
				# UNLESS there is another chord with the same spelling
				if get_index_of_best_matching_target_chord() == -1:
					reset_birdie()

			# make sure we set up target_chords removal BEFORE running create -_-
			# if the player has completed all target chords, give them more (aka next round)
			if get_target_chords_remaining() == 0:
				print("Next 'level' setup due to player action")
				# make a new set of chords
				create_target_chords()
				change_score_by(SCORE_ALL_TARGET_CHORDS)
			else:
				change_score_by(SCORE_SINGLE_TARGET_CHORD)
#
# END GOAL
#

#
# BIRDIE
#
func start_birdie():
	reset_birdie()
	$Birdie/BirdWhistleTimer.start()

func reset_birdie() -> void:
	# start anew
	reset_birdie_vars()
	$Birdie/ChordDisplay.reset_note_displays()

func reset_birdie_vars() -> void:
	# start anew
	birdie_notes = []
	birdie_current_note_of_target_chord_index = 0
	birdie_current_target_chord_index = null

func get_next_birdie_target_index() -> int:
		# if there are any chords for birdie to score on
	if get_target_chords_remaining() > 0:
		if birdie_current_target_chord_index == null:
			# we're not targeting anything yet
			# var target_chord_index = 
			return get_next_available_target_chord_index()
			# birdie_current_target_chord_index = target_chord_index
		elif target_chords[birdie_current_target_chord_index] == null:
			# It is possible that there was a C Major (CEG) in the first index
			# but the player already caught that one
			# and there is also a C Major in the 3rd index, so we can still finish it
			# so we can't just rely on this index.
			# Instead we need to see if there are any other chords with the same spelling 
			# with which we can target
			# the one we're target was taken by the player
			var target_chord_index = get_index_of_best_matching_target_chord()
			if target_chord_index != -1:
				# there is another chord we can target now to beat the player
				return target_chord_index
			else:
				target_chord_index = get_next_available_target_chord_index()
				if target_chord_index != -1:
					return target_chord_index
	# print("Nothing for birdie to target")
	return -1

func _on_BirdWhistleTimer_timeout() -> void:
	# don't start until the player has at least scored once
	if !first_score_occurred:
		return

	# if there are any chords for birdie to score on
	if get_target_chords_remaining() > 0:
		var target_chord_index = get_next_birdie_target_index()
		if target_chord_index > -1:
			birdie_current_target_chord_index = target_chord_index

		var target_chord = target_chords[birdie_current_target_chord_index]
		if target_chord != null:
			var next_note = target_chord[birdie_current_note_of_target_chord_index]
			$Birdie/ChordDisplay.set_next_available_note(next_note)
			AudioManager.play_whistle(next_note)
			birdie_notes.append(next_note)
			# reset counter if necessary
			if birdie_current_note_of_target_chord_index >= 2:
				# This condition means birdie just finished a chord!
				$HUD.remove_chord_at_index(birdie_current_target_chord_index)
				# we can't simply remove(), we want this array to stay the right size
				# I do not like this. Needs a smarter, centralized game state storage.
				target_chords[birdie_current_target_chord_index] = null
				# print("Birdie scored!")
				# print("Removed target chord at index " + String(birdie_current_target_chord_index), target_chords)
				change_score_by(-SCORE_SINGLE_TARGET_CHORD)
				# start anew
				reset_birdie()

				# If we just killed the last target chord, give the player and the bird some new ones
				if get_target_chords_remaining() == 0:
					# make a new set of chords
					print("Next 'level' setup due to Birdie action")
					create_target_chords()
			else:
				birdie_current_note_of_target_chord_index += 1
#
# END BIRDIE
#

#
# SCORE
#
func reset_score() -> void:
	score = 0
	$HUD.set_score(0)

func change_score_by(num: int) -> void:
	score += num
	$HUD.set_score(score)
	if score < 0:
		end_game()

	# in order to start birdie, you must have scored at least once to "wake her up"
	if first_score_occurred == false:
		first_score_occurred = true
#
# END SCORE
#

#
# UTILITY FUNCTIONS
#
func get_target_chords_remaining() -> int:
	var remaining = 0;
	for i in range(0, target_chords.size()):
		if target_chords[i] != null:
			remaining += 1
	return remaining

func get_next_available_target_chord_index():
	for i in range(0, target_chords.size()):
		if target_chords[i] != null:
			return i

# if birdie has [C, E] and there is a [C, E, G] available, we want that index
# if birdie has [D] and there is a [D, F, A] available, we want that index
# always prefer the "closest" possible match. So if birdie has 3 notes, look for
# a full chord. Otherwise, seek the biggest partial, and then the single note
func get_index_of_best_matching_target_chord() -> int:
	var birdie_note_size = birdie_notes.size()
	# print("Birdie has " + String(birdie_note_size) + " notes", birdie_notes, target_chords)
	for i in range(0, target_chords.size()):
		var target_chord = target_chords[i]
		if target_chord != null:
			# gonna do a strange cascade here
			if birdie_note_size > 0 && birdie_notes[0] == target_chord[0]:
				if birdie_note_size > 1 && birdie_notes[1] == target_chord[1]:
					if birdie_note_size > 2 && birdie_notes[2] == target_chord[2]:
						# print("Found birdie a 3-note potential match", target_chord, birdie_notes)
						return i
					# print("Found birdie a 2-note potential match", target_chord, birdie_notes)
					return i
				# print("Found birdie a 2-note potential match", target_chord, birdie_notes)
				return i
	return -1

func get_chord_index_in_target_chords(chord: Array) -> int:
	for i in range(0, target_chords.size()):
		var target_chord = target_chords[i]
		if (
			# it is possible we removed the chord at this index
			target_chord != null
			&& chord[0] == target_chord[0]
			&& chord[1] == target_chord[1]
			&& chord[2] == target_chord[2]
		):
			# print("Found Target Chord", target_chord, chord)
			return i
	return -1
#
# END UTILITY FUNCTIONS
#
