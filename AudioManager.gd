extends Node

# in the future, support multiple instruments
var instrument_types = ["clean_guitar"]

# to play the music layer, background
var background_music_player: AudioStreamPlayer

# current instrument
var instrument = instrument_types[0]

# we should need at most 3 players to play a chord,
# but also some for sound effects. We keep extra available
const MAX_STREAMS = 10

# the finish signal doesn't provide audio stream player instace
# we need to keep track of which ones are currently being used
# this way when a finished signal is emitted, we can return 
# them to the open pool
var active_stream_players = []

# unused (free) pool
var free_stream_players = []

# if we can't acquire a stream we will fallback to default
var default_stream_player: AudioStreamPlayer

func highest_to_lowest(a: int, b: int):
	if a > b:
		return true
	return false


func release_unused_players():
	# print("Attempting to release player...")
	var indexes_to_remove = []
	for index in range(0, active_stream_players.size()):
		var player = active_stream_players[index]
		var is_playing = player.is_playing()
		if player != null && !is_playing:
			# var index = active_stream_players.bsearch(player)
			# print("Found finished player", player, "Index: " + str(index))
			if index >= 0:
				# print("Removing player from active array")
				indexes_to_remove.append(index)
	# reverse them to avoid array resizing problems
	indexes_to_remove.sort_custom(self, "highest_to_lowest")
	for index in indexes_to_remove:
		var player = active_stream_players[index]
		free_stream_players.append(player)
		active_stream_players.remove(index)

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(MAX_STREAMS):
		var audio_stream_player = AudioStreamPlayer.new()
		audio_stream_player.autoplay = false
		audio_stream_player.connect("finished", self, "release_unused_players")
		free_stream_players.append(audio_stream_player)
		# audio won't play if stream is not in the tree
		add_child(audio_stream_player)

	default_stream_player = AudioStreamPlayer.new()
	if default_stream_player == null:
		# print("No default stream")
		pass
	else:
		add_child(default_stream_player)
	
	background_music_player = AudioStreamPlayer.new()
	# Looping doesn't work in godot, so we do it manually.
	background_music_player.connect("finished", background_music_player, "play")
	add_child(background_music_player)

func acquire_player():
	var audio_stream_player = free_stream_players.pop_back()
	if audio_stream_player != null:
		active_stream_players.append(audio_stream_player)
		return audio_stream_player

func play_music(filename, bus_name = "DefaultBus"):
	if background_music_player != null:
		background_music_player.stream = load(filename)
		background_music_player.bus = bus_name
		background_music_player.play()
	else:
		# print("Attempting to play when player is uninitialized.")
		pass

func play_note(note, octave, bus_name = "DefaultBus"):
	var target_player = acquire_player()
	if target_player != null:
		target_player.stream = load("res://sfx/instruments/" + instrument + "/" + note + str(octave) + ".wav")
		target_player.bus = bus_name
		target_player.play()
		
func play_whistle(note, octave = 5, bus_name = "BirdieBus"):
	var target_player = acquire_player()
	if target_player != null:
		target_player.stream = load("res://sfx/instruments/birdie/" + note + str(octave) + ".wav")
		target_player.bus = bus_name
		target_player.play()

func play_notes(notes):
	if notes.size() > MAX_STREAMS:
		# print("Too many notes.")
		pass
	elif notes.size() < 0:
		# print("Too few notes.")
		pass

	for i in range(notes.size()):
		var note = notes[i]
		var target_bus = "InstrumentBus" + str(i)
		play_note(note.letter, note.octave, target_bus)

func stop_all():
	background_music_player.stop()
	default_stream_player.stop()
	for player in active_stream_players:
		player.stop()
	release_unused_players()