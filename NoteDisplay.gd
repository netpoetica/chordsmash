extends Node2D

var current_note = null
var note_types = ["C", "D", "E", "F", "G", "A", "B"]
	
func set_note(note_name: String):
	if note_types.has(note_name):
		current_note = note_name
		var texture  = load("res://gfx/notes/notes_" + note_name + ".png")
		$Sprite.texture = texture
	else:
		print("Invalid note name specified.")
