extends Node2D

signal instructions_read

func _ready():
	$Button.connect("pressed", self, "finish")

func finish():
	emit_signal("instructions_read")
