extends Object

# consider - this should probably be called Triad, saving
# chord for being more sophisticated. Or maybe a Triad is
# just a chord with MAX_NOTES = 3
class_name Chord

# version 1 supports only 3-note chords
const MAX_NOTES := 3

# there are many other types of chords that will need to be added
enum ChordType {
	Major,
	Minor,
	Diminished
}

const CHORD_TYPES = [ ChordType.Major, ChordType.Minor, ChordType.Diminished ]

var name: String setget set_name
var root: Note setget set_root
var chord_type: int setget set_chord_type
var notes: Array = [] setget ,get_notes

func add_note(note: Note) -> void:
	notes.append(note)

func get_notes() -> Array:
	return notes

func set_name(val: String) -> void:
	name = val
	
func set_root(val: Note) -> void:
	root = val

func set_chord_type(val: int) -> void:
	if CHORD_TYPES.has(val):
		chord_type = val

func set_major() -> void:
	set_chord_type(ChordType.Major)

func set_minor() -> void:
	set_chord_type(ChordType.Minor)

func set_diminished() -> void:
	set_chord_type(ChordType.Diminished)

