extends Object

class_name Scale

const C_MAJOR = ["C", "D", "E", "F", "G", "A", "B" ]

# as in ["C", "D", "E" ... ]
var note_letters: Array = []

func use_scale(scale: Array) -> void:
	note_letters = scale;

func get_note_from_scale(index: int, offset_interval: int):
	var target_index = index + offset_interval
	var scale_length = note_letters.size() # should be 7 for most scales
	if (target_index >= scale_length):
		# loop back around to the front
		# in case user asks for a rediculous distance - we can keep looping around
		var num_circles = int(floor(target_index / scale_length))
		var remainder_index = target_index - (scale_length * num_circles)
		if remainder_index <= scale_length:
			return note_letters[remainder_index]
	else:
		return note_letters[target_index]

func get_triad_from_root(root_note: String) -> Array:
	var root_index = note_letters.find(root_note)
	# we think of 3 as a "third", but array is zero-indexed
	var third = get_note_from_scale(root_index, 2)
	var fifth = get_note_from_scale(root_index, 4)
	return [root_note, third, fifth]