extends Object

class_name ChordsDataObject

# array of Chord object
var chords: Array = [] setget ,get_chords

func _init():
	# First create all the notes
	var c_note = Note.new()
	c_note.set_letter("C")
	var d_note = Note.new()
	d_note.set_letter("D")
	var e_note = Note.new()
	e_note.set_letter("E")
	var f_note = Note.new()
	f_note.set_letter("F")
	var g_note = Note.new()
	g_note.set_letter("G")
	var a_note = Note.new()
	a_note.set_letter("A")
	var b_note = Note.new()
	b_note.set_letter("B")
	
	# C Major through B Diminished
	var c_major = Chord.new()
	c_major.set_name("C Major")
	c_major.set_major()
	c_major.set_root(c_note)
	c_major.add_note(c_note)
	c_major.add_note(e_note)
	c_major.add_note(g_note)
	
	var d_minor = Chord.new()
	d_minor.set_name("D Minor")
	d_minor.set_minor()
	d_minor.set_root(d_note)
	d_minor.add_note(d_note)
	d_minor.add_note(f_note)
	d_minor.add_note(a_note)
	
	var e_minor = Chord.new()
	e_minor.set_name("E Minor")
	e_minor.set_minor()
	e_minor.set_root(e_note)
	e_minor.add_note(e_note)
	e_minor.add_note(g_note)
	e_minor.add_note(b_note)
	
	var f_major = Chord.new()
	f_major.set_name("F Major")
	f_major.set_major()
	f_major.set_root(f_note)
	f_major.add_note(f_note)
	f_major.add_note(a_note)
	f_major.add_note(c_note)
	
	var g_major = Chord.new()
	g_major.set_name("G Major")
	g_major.set_major()
	g_major.set_root(g_note)
	g_major.add_note(g_note)
	g_major.add_note(b_note)
	g_major.add_note(d_note)
	
	var a_minor = Chord.new()
	a_minor.set_name("A Minor")
	a_minor.set_minor()
	a_minor.set_root(a_note)
	a_minor.add_note(a_note)
	a_minor.add_note(c_note)
	a_minor.add_note(e_note)
	
	var b_diminished = Chord.new()
	b_diminished.set_name("B Diminished")
	b_diminished.set_diminished()
	b_diminished.set_root(b_note)
	b_diminished.add_note(b_note)
	b_diminished.add_note(d_note)
	b_diminished.add_note(f_note)
	
	# append to array
	chords.append(c_major)
	chords.append(d_minor)
	chords.append(e_minor)
	chords.append(f_major)
	chords.append(g_major)
	chords.append(a_minor)
	chords.append(b_diminished)

func get_chords() -> Array:
	return chords

func get_chord_by_name(target_name: String):
	for chord in chords:
		if chord.name == target_name:
			return chord

func get_chord_by_notes_arr(notes_arr: Array):
	for chord in chords:
		var notes = chord.get_notes()
		if (
			notes[0].letter == notes_arr[0]
			&& notes[1].letter == notes_arr[1]
			&& notes[2].letter == notes_arr[2]
		):
			return chord