extends Object

class_name Note

# future versions will support more octaves
const SUPPORTED_OCTAVES: Array = [3, 4]
const DEFAULT_OCTAVE = 3
const NOTE_NAMES = ["C", "D", "E", "F", "G", "A", "B" ]

var letter: String setget set_letter
var octave: int = DEFAULT_OCTAVE setget set_octave

func set_letter (val: String) -> void:
	if NOTE_NAMES.has(val):
		letter = val

func set_octave (val: int) -> void:
	if SUPPORTED_OCTAVES.has(val):
		octave = val
	else:
		octave = 3 
