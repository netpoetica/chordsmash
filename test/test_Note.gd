extends "res://addons/gut/test.gd"
# const Note = preload("res://classes/Note.gd")

func test_note_octave_defaults_to_3():
	var note = Note.new()
	assert_not_null(note.octave, "should not be empty")
	assert_eq(note.octave, note.DEFAULT_OCTAVE, "should default to 3")

func test_note_octave_cannot_be_set_to_unspported():
	var note = Note.new()
	note.set_octave(-9999)
	assert_eq(note.octave, note.DEFAULT_OCTAVE, "should use default octave when invalid octave passed")

func test_all_supported_octaves_can_be_set():
	var note = Note.new()
	for octave in note.SUPPORTED_OCTAVES:
		note.set_octave(octave)
		assert_eq(note.octave, octave, "should be able to set octave to supported octave")
