extends "res://addons/gut/test.gd"
# var AudioManager = preload("res://AudioManager.gd")

func test_audio_manager_state():
	assert_ne(AudioManager.default_stream_player, null, "should have a default/fallback player for all audio")
	assert_ne(AudioManager.background_music_player, null, "should have a dedicated player for background music")
	
	assert_eq(AudioManager.free_stream_players.size(), AudioManager.MAX_STREAMS, "should have the specified number of audio stream players available and ready")
	assert_eq(AudioManager.active_stream_players.size(), 0, "should have no active players at startup")
	
	assert_eq(AudioManager.instrument, "clean_guitar", "should have a default instrument of guitar")
	
func test_audio_stream_pool():
#	var player = AudioManager.acquire_player()
#	assert_ne(player, null, "should be able to acquire a player")
#	assert_eq(AudioManager.active_stream_players.size(), 1, "should have 1 active stream player")
#	assert_eq(AudioManager.free_stream_players.size(), AudioManager.MAX_STREAMS - 1, "should have 1 less than pool size players after acquire")
#
#	AudioManager.release_unused_players()
	# assert_eq(AudioManager.free_stream_players.size(), AudioManager.MAX_STREAMS, "should have max free after releasing 1 acquired player")
	for i in range(0, AudioManager.MAX_STREAMS):
		AudioManager.acquire_player()
	assert_eq(AudioManager.active_stream_players.size(), AudioManager.MAX_STREAMS, "should have max active after acquiring max players")
	assert_eq(AudioManager.free_stream_players.size(), 0, "shouldn't have any free streams after acquiring max")
	AudioManager.release_unused_players()
	assert_eq(AudioManager.active_stream_players.size(), 0, "should have no active after releasing max acquired player")
	assert_eq(AudioManager.free_stream_players.size(), AudioManager.MAX_STREAMS, "should have max free after releasing max acquired player")
	
	