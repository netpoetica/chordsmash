extends "res://addons/gut/test.gd"
var Main = preload("res://Main.gd")

# instance scene for testing
var main = null

func before_each():
	main = Main.new()
	main.setup_target_chords()

func test_populate_target_chords():
	assert_eq(main.target_chords.size(), 4, "should start with 0 target chords")
	main.populate_target_chords()
	assert_eq(main.target_chords.size(), 4, "should have 4 target chords")
	for chord in main.target_chords:
		assert_ne(chord, null, "chords should not be null")

func test_get_remaining_target_chords():
	main.populate_target_chords()
	var num_remaining = main.get_target_chords_remaining()
	assert_eq(num_remaining, 4, "should have a total of four chords remaining")
	main.target_chords = [];
	main.target_chords.resize(main.NUM_TARGET_CHORDS)
	num_remaining = main.get_target_chords_remaining()
	assert_eq(num_remaining, 0, "should have a total of 0 chords remaining")

func test_get_next_target_chord():
	main.populate_target_chords()
	var chord_at_index_1 = main.target_chords[0]
	var next_available_chord_index = main.get_next_available_target_chord_index()
	var next_available_chord = main.target_chords[next_available_chord_index]
	assert_eq(chord_at_index_1, next_available_chord, "should get chord at first index")

	for chord in main.target_chords:
		var chord_index = main.get_chord_index_in_target_chords(chord)
		assert_eq(chord, main.target_chords[chord_index], "should return correct chord at index")

func test_birdie_in_happy_path_target_chord_sequence():
	main.populate_target_chords()
	main.reset_birdie_vars()
	
	# when all four chords are present
	var next_index = main.get_next_birdie_target_index()
	assert_eq(main.target_chords[next_index], main.target_chords[0], "should get first chord in target chords if it is open")

	# when only first chord is missing
	main.target_chords[0] = null
	next_index = main.get_next_birdie_target_index()
	assert_eq(main.target_chords[next_index], main.target_chords[1], "should get second chord in target chords when first is removed")

	# when first and second chord are missing
	main.target_chords[1] = null
	next_index = main.get_next_birdie_target_index()
	assert_eq(main.target_chords[next_index], main.target_chords[2], "should get third chord in target chords when first and second are removed")

	# when first, second and third chord are missing
	main.target_chords[2] = null
	next_index = main.get_next_birdie_target_index()
	assert_eq(main.target_chords[next_index], main.target_chords[3], "should get third chord in target chords when first and second are removed")

	# when all chords are gone, birdie should get a -1 and do nothing
	main.target_chords[3] = null
	next_index = main.get_next_birdie_target_index()
	assert_eq(next_index, -1, "should get -1 when there are no chords")

func test_birdie_when_birdie_has_partial_chords():
	main.target_chords[0] = ["C", "E", "G"]
	main.target_chords[1] = ["A", "C", "E"]
	main.target_chords[2] = ["C", "E", "G"]
	main.target_chords[3] = ["A", "C", "E"]
	main.reset_birdie_vars()
	# we start trying to target C E G at index 0
	main.birdie_current_target_chord_index = 0
	# the birdie has already whistled C and E
	main.birdie_notes = ["C", "E"]
	# so that are at index 1 of the C chord at index 0
	main.birdie_current_note_of_target_chord_index = 1
	
	# now the player completes target_chords[0]
	main.target_chords[0] = null
	
	# so birdie should still be able to target chord at index 1 which is also C E G
	assert_eq(main.get_next_birdie_target_index(), 2, "should begin targeting the next possible C major chord if player completes birdie's target C major chord")
	
	# if we remove the next  C E G at index 2
	main.target_chords[2] = null
	assert_eq(main.get_next_birdie_target_index(), 1, "should begin targeting the next possible A chord (ACE) if player completes all C E G chords and birdie has C E in their current notes")
	
	# if the A chord at index 1 is removed, we should get index 3
	main.target_chords[1] = null
	assert_eq(main.get_next_birdie_target_index(), 3, "should begin targeting the next possible A chord (ACE) if player completes all C E G chords and first A C E and birdie has C E in their current notes")
