extends "res://addons/gut/test.gd"

func test_should_get_note_index_by_letter_name():
	var scale = Scale.new()
	scale.use_scale(Scale.C_MAJOR)
	# 1 (root), C E G. 1 _ 3 _ 5
	assert_eq(scale.get_note_from_scale(0, 2), "E", "should have note E a third above C")
	assert_eq(scale.get_note_from_scale(0, 4), "G", "should have note G a third above C")
	assert_eq(scale.get_note_from_scale(0, 6), "B", "should have note B a seventh above C")
	assert_eq(scale.get_note_from_scale(0, 7), "C", "should have note C an octave above C")
	assert_eq(scale.get_note_from_scale(6, 7), "B", "should have note B an octave above B in the key of C")
	assert_eq(scale.get_note_from_scale(6, 14), "B", "should have note B two octaves above B in the key of C")
	assert_eq(scale.get_note_from_scale(6, 21), "B", "should have note B three octaves above B in the key of C")

func test_should_get_chords_from_root():
	var scale = Scale.new()
	scale.use_scale(Scale.C_MAJOR)
	
	assert_eq(scale.get_triad_from_root("C"), ["C", "E", "G"], "should get C Major from C in the key of C")
	assert_eq(scale.get_triad_from_root("D"), ["D", "F", "A"], "should get D Minor from D in the key of C")
	assert_eq(scale.get_triad_from_root("E"), ["E", "G", "B"], "should get E Minor from E in the key of C")
	assert_eq(scale.get_triad_from_root("F"), ["F", "A", "C"], "should get F Major from F in the key of C")
	assert_eq(scale.get_triad_from_root("G"), ["G", "B", "D"], "should get G Major from G in the key of C")
	assert_eq(scale.get_triad_from_root("A"), ["A", "C", "E"], "should get A Minor from A in the key of C")
	assert_eq(scale.get_triad_from_root("B"), ["B", "D", "F"], "should get B Diminished from B in the key of C")
	