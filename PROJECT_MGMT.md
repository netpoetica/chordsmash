

This document loosely guides remaining tasks for Chordsmash.


# TODO
Fin!

# DONE
- create a full design in Illustrator
- add background image
- create functionality for levels (JSON config)
	X This is done via new chords falling in
- add a HUD that indicates which chords need to be played in the level to win the level
- fix bug where tapping and releasing a note causes it to disappear
- add a Chord Dictionary
- add background music
- add a tutorial mode via the ? button in the HUD
	- it should explain to some degree what the user is expected to do
- main menu
	- new game
	- tutorial
	- credits
- figure out how to lose: 
	- maybe the bird slowly whistles notes, that make their way into forming a chord. If it makes the chord before you do, then you lose the round and lose ROUND_SCORE points. If you reach below 0, you lose! And the bird wins :D
- make player lose when their score reaches below 0 
	- or maybe the bird doesn't start whistling until the player smashes a chord?
- fix bug with extra, untrackable target_chords as you play (when I reached a score of 651, I saw 5 chords on the target chords indicator)
	- this was a race condition due to removal and addition of node vs. physics and queuing to free

# Not Going to Happen
- fix click on loop of background music -_-
- tutorial should run automatically on first run
- create a function that determines the optimal octave to play notes in. For example, if you give me CEG, C should be the root. If you give me GBD, G should be the root. However, it should be G3, B3, and D4 - if you play D4, it will sound like a root
- main menu
	- config
- create some kind of indication that the chords succesfully smashed (other than audio)
	- maybe some particple effect
	- when the chords smash, make the screen shake
- animate the bird character like so:
	- she is asleep on her branch
	- when you click your first note, she opens one eye
	- when you click your second note, if it can make a chord, she perks up
	- when you click your third note, if it is a chord, she bounces around happily or smiles
	- when you smash notes into a chord, she celebrates
