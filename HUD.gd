extends CanvasLayer

signal help_requested

onready var ChordDisplay = preload("res://ChordDisplay.tscn")

const NUM_SPAWN_POSITIONS = 4
var spawn_positions: Array

func _ready():
	# it is easier to loop through them
	spawn_positions = [
		$ChordSpawnPosition1,
		$ChordSpawnPosition2,
		$ChordSpawnPosition3,
		$ChordSpawnPosition4
	]
	
	# add initial children that we will change later
	for spawn_position in spawn_positions:
		var chord_display = ChordDisplay.instance()
		chord_display.hide()
		spawn_position.add_child(chord_display)
	
	$HelpButton.connect("pressed", self, "ask_for_help")

func ask_for_help():
	emit_signal("help_requested")

func set_score(num):
	$PointsLabel.text = str(num)

func set_chords(triads, deferred = false):
	# print("set chords. Deferred?" + String(deferred))
	for i in range(0, triads.size()):
		var spawn_position = spawn_positions[i]
		var chord_display = spawn_position.get_node('ChordDisplay')
		if spawn_position != null && chord_display != null:
			var triad = triads[i]
			if triad != null:
				# print("Setting ChordDisplay to a new triad")
				chord_display.set_display(triad)
				chord_display.apply_central_impulse(Vector2.UP * 100)
				chord_display.show()

func remove_chord_at_index(index: int):
	if index >= NUM_SPAWN_POSITIONS:
		# print("Invalid index")
		return

	var spawn_position: Node2D = spawn_positions[index]
	if spawn_position != null:
		var chord_display = spawn_position.get_node('ChordDisplay')
		if chord_display != null:
			# print("Removing chord from index " + String(index) + " in the HUD")
			# if we hide it then we won't have to worry about the next one falling
			# on top of it in the HUD *before* queue free is called
			chord_display.hide()
			# chord_display.queue_free()

func clear_chords():
	for i in range(0, NUM_SPAWN_POSITIONS):
		remove_chord_at_index(i)
