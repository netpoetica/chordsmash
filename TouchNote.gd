extends RigidBody2D

# Declare member variables here. Examples:
export var min_speed = 100
export var max_speed = 250

var note_types = ["C", "D", "E", "F", "G", "A", "B"]
var note
var state
var touch_ptr_id

# when a note is free, it will move
# when it is grabbed, it will follow finger position
enum state_types {
	free
	grabbed
}

var previous_linear_velocity 

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	# use a different letter every time
	note = note_types[randi() % note_types.size()]
	state = state_types.free
	var texture  = load("res://gfx/notes/notes_" + note + ".png")
	$Sprite.texture = texture

func _on_VisibilityNotifier2D_screen_exited():
	# print("Freeing a TouchNote", self)
	queue_free()

func _process(delta):
#	if mode == RigidBody2D.MODE_RIGID && state == state_types.grabbed:
#		mode = RigidBody2D.MODE_STATIC
#	elif mode == RigidBody2D.MODE_STATIC && state == state_types.free:
#		mode = RigidBody2D.MODE_RIGID
	# TODO: why does this not stop from being removed?
#	if touch_ptr_id == null && (linear_velocity.x <= 0 and linear_velocity.y <= 0):
#		queue_free()
	pass
	
func _physics_process(delta):
	var accelerometer = Input.get_accelerometer()
	# X is fine but Y is inverted
	var force_direction = Vector2(accelerometer.x, -accelerometer.y).normalized()
	self.linear_velocity += force_direction
	
func grab(ptr_id, touch_position):
	# print("grab", ptr_id)
	state = state_types.grabbed
	touch_ptr_id = ptr_id
	# stop movement
	previous_linear_velocity = linear_velocity
	linear_velocity = Vector2(0, 0)
	# position = touch_position
	# play_sound()
		
#func play_sound():
#	var instrument_player = get_node("/root/AudioManager")
#	if instrument_player != null:
#		instrument_player.play_note(note, 3, "InstrumentBus")

func release(ptr_id):
	linear_velocity = previous_linear_velocity
	touch_ptr_id = null
	state = state_types.free
